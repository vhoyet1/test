window.onload = async function() {
    const searchBar = document.getElementById('search-bar')
    searchBar.value = "";
    const completion = document.getElementById('completion-list')

    let response = await fetch("https://hwloc-xmls.herokuapp.com")
    let xmls = await response.json()
    let response2 = await fetch("https://hwloc-xmls.herokuapp.com/json")
    let xmls_infos = await response2.json()
    completionTags = setCompletionTags(xmls, xmls_infos)

    /*set tag to default value*/
    for(let i = 1 ; i <= 4 ; i++ ){
        let container = document.getElementById("filter-element" + i)
        container.firstElementChild.value = "default"
        container.lastElementChild.value = null
    }
    
    searchBar.addEventListener('keyup',function(event){
        if(event.keyCode === 13){
            searchBar.blur()
            const content = document.getElementById('content')
            deleteChildren(content)
            event.preventDefault()
            search()
        }
    })
    
    searchBar.addEventListener('input', function(event){
        deleteChildren(completion)
        let searched = searchBar.value

        complete(searched)

        /* if completion is not yet visible and there is something to complete, make it visible */
        if(completion.classList.contains("invisible") && completion.lastElementChild)
            completion.classList.toggle("invisible")
        else if(!completion.classList.contains("invisible") && !completion.lastElementChild)
            completion.classList.toggle("invisible")
    })

    searchBar.addEventListener('focus', function(event){
        deleteChildren(completion)
        complete(searchBar.value)
        if(completion.lastElementChild)
            completion.classList.toggle("invisible")
    })

    searchBar.addEventListener('focusout', function(event){
        /* check if the target is a completion item*/
        if(event.explicitOriginalTarget && 
        event.explicitOriginalTarget.parentElement && 
        event.explicitOriginalTarget.parentElement.classList.contains("completion-item") &&
        completion.lastElementChild)
            return
        if(!completion.classList.contains("invisible"))
            completion.classList.add("invisible")
    })

    async function search(){
        let selects = document.getElementById('filter').children
        let tags = {}
        let searched
    
        /* get selected tag */
        for( let i = 0 ; i < selects.length ; i++){
            if(selects[i].firstElementChild.value && selects[i].lastElementChild.value)
                tags[selects[i].firstElementChild.value] = selects[i].lastElementChild.value
        }
    
        searched = document.getElementById('search-bar').value
        tags = parseSearch(searched, tags, completionTags)
        
        let response = await fetch("https://hwloc-xmls.herokuapp.com/tags/" + encodeURI(JSON.stringify(tags)))
        let data = await response.json()
    
        const content = document.getElementById('content')
        deleteChildren(content)
        data.xml.forEach(xml => {
            let title = xml.title.substring(0, xml.title.length-4)
            const span = document.createElement('span')
            const a = document.createElement('a')
            let href = xml.title
            while(href.includes('+')){
                href = href.replace('+', '')
            }
            a.href = "https://hwloc-xmls.herokuapp.com/xml/" + href
            a.innerHTML = xml.title
            a.classList.add('zoomIn')
            a.classList.add('animated')
    
            const div = document.createElement("div")
            const p = document.createElement('p')
            div.classList.add("info-popup", "invisible")
            p.innerHTML = xml.title + "<br>"
            for(var key in xmls_infos[title]){
                p.innerHTML += key + ": " + xmls_infos[title][key] + "<br>"
            }
    
            a.addEventListener("mouseover", function(event){
                toggleInvisibleOnPopup(div)
            })
            a.addEventListener("mouseout", function(event){
                toggleInvisibleOnPopup(div)
            })

            span.appendChild(a)
            div.appendChild(p)
            content.appendChild(span)
            content.appendChild(div)
        });
    }

    /* call a first search without parameters to display all xmls */
    search()
}



function deleteChildren(element){
    while(element.firstChild)
        element.removeChild(element.firstChild)
}

function complete(searched){
    if(!searched.includes(':')){
        addCompletionItems(completionTags, searched)
    } else {
        completeTag(searched)
    }
}

function completeTag(searched, prefix = ""){
    if(searched.includes(';')){
        let tags = searched.split(';')
        let prefix = tags.join("").replace(tags[tags.length - 1], "") + ";"
        completeTag(tags[tags.length - 1], prefix)
    } else if(!searched.includes(':')){
        createCompletionItemsByTag("tag", searched, prefix)
    }else {
        let tag = searched.split(':')
        prefix = prefix + tag[0] + ":"
        createCompletionItemsByTag(tag[0], tag[1], prefix)
    }
}

function completion_item_added(tag, item){
    for(let info of tag){
        if(info == item)
            return true
    }
    return false
}

function setCompletionTags(xmls, xmls_infos){
    possibleItems = {title : new Array(), chassis : new Array(), architecture : new Array(), "uname-architecture" : new Array(),
        "sub-architecture" : new Array(), "cpu-vendor" : new Array(), "cpu-family" : new Array(), tag : new Array()}

    possibleItems.tag.push("architecture:")
    possibleItems.tag.push("uname-architecture:")
    possibleItems.tag.push("sub-architecture:")
    possibleItems.tag.push("chassis:")
    possibleItems.tag.push("cpu-vendor:")
    possibleItems.tag.push("cpu-family:")

    xmls.xml.forEach(xml => {
        /* removes the ".xml" at the end of the title */
        xml = xml.title.substring(0, xml.title.length-4)

        possibleItems.title.push(xml)
        
        if(xmls_infos[xml].architecture && !completion_item_added(possibleItems.architecture, xmls_infos[xml].architecture))
            possibleItems.architecture.push(xmls_infos[xml].architecture)

        if(xmls_infos[xml]["uname-architecture"] && !completion_item_added(possibleItems["uname-architecture"], xmls_infos[xml]["uname-architecture"]))
            possibleItems["uname-architecture"].push(xmls_infos[xml]["uname-architecture"])

        if(xmls_infos[xml]["sub-architecture"] && !completion_item_added(possibleItems["sub-architecture"], xmls_infos[xml]["sub-architecture"]))
            possibleItems["sub-architecture"].push(xmls_infos[xml]["sub-architecture"])

        if(xmls_infos[xml]["cpu-vendor"] && !completion_item_added(possibleItems["cpu-vendor"], xmls_infos[xml]["cpu-vendor"]))
            possibleItems["cpu-vendor"].push(xmls_infos[xml]["cpu-vendor"])

        if(xmls_infos[xml]["cpu-family"] && !completion_item_added(possibleItems["cpu-family"], xmls_infos[xml]["cpu-family"]))
            possibleItems["cpu-family"].push(xmls_infos[xml]["cpu-family"])

        if(xmls_infos[xml]["chassis"] && !completion_item_added(possibleItems["chassis"], xmls_infos[xml]["chassis"]))
            possibleItems["chassis"].push(xmls_infos[xml]["chassis"])
    })

    return possibleItems
}


function addCompletionItems(completionTags, searched){
    createCompletionItemsByTag("title", searched)
    createCompletionItemsByTag("architecture", searched)
    createCompletionItemsByTag("uname-architecture", searched)
    createCompletionItemsByTag("sub-architecture", searched)
    createCompletionItemsByTag("cpu-vendor", searched)
    createCompletionItemsByTag("cpu-family", searched)
    createCompletionItemsByTag("chassis", searched)
    createCompletionItemsByTag("tag", searched)
}

function createCompletionItemsByTag(tag, searched, prefix = false){
    completionTags[tag].forEach(item => {
        if(item.includes(searched))     
            createCompletionItem(item, prefix)
    })
}

function createCompletionItem(item, prefix){
    const completion = document.getElementById('completion-list')
    const searchBar = document.getElementById('search-bar')

    let completion_item = document.createElement('p')
    completion_item.innerHTML = item
    completion_item.classList.add("completion-item")
    completion_item.addEventListener('click', function(event){
        if(prefix)
            searchBar.value = prefix + item
        else
            searchBar.value = event.target.innerHTML
        completion.classList.toggle("invisible")
    })
    completion.appendChild(completion_item)
}

function infoSearched(tags, completionTags, searched){
    let tag

    if(completionTags.title.includes(searched))
        tag = "title"
    else if(completionTags["architecture"].includes(searched))
        tag = "architecture"
    else if(completionTags["uname-architecture"].includes(searched))
        tag = "uname-architecture"
    else if(completionTags["sub-architecture"].includes(searched))
        tag = "sub-architecture"
    else if(completionTags["cpu-family"].includes(searched))
        tag = "cpu-family"
    else if(completionTags["cpu-vendor"].includes(searched))
        tag = "cpu-vendor"
    else if(completionTags["chassis"].includes(searched))
        tag = "chassis"

    tags[tag] = searched
    return tags
}

function parseSearch(searched, tags, completionTags){
    if(!searched.includes(':')){
        tags = infoSearched(tags, completionTags, searched)
        return tags
    }else if(searched.includes(';')){
        searched = searched.split(';')
        for(let i = 0 ; i < searched.length ; i ++){
            searched[i] = searched[i].split(':')
            tags[searched[i][0]] = searched[i][1]
        }
    } else {
        searched = searched.split(':')
        tags[searched[0]] = searched[1]
    }
    
    return tags
}

function toggleInvisibleOnPopup(element){
    console.log(element)
    element.classList.toggle('invisible')
}

function setNextVisible(event){
    const select = event.target
    if(select.parentElement.id == "filter-element1" && document.getElementById("filter-element2").classList.contains("invisible"))
        document.getElementById("filter-element2").classList.remove("invisible")
    else if(select.parentElement.id == "filter-element2" && document.getElementById("filter-element3").classList.contains("invisible"))
        document.getElementById("filter-element3").classList.remove("invisible")
    else if(select.parentElement.id == "filter-element3" && document.getElementById("filter-element4").classList.contains("invisible"))
        document.getElementById("filter-element4").classList.remove("invisible")

    deleteOption(select.selectedIndex, select.parentElement.id[select.parentElement.id.length - 1])
}

function deleteOption(selected, id){
    let i = id
    i++
    for( i ; i <= 4 ; i++ ){
        let container = document.getElementById("filter-element" + i)
        container.firstElementChild.options[selected].remove()
    }
}
