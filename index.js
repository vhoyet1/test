const express = require("express")
const app = express()
const cors = require('cors')
const testFolder = './xml/'
const fs = require('fs')
const path = require('path')

app.use(cors())

app.get("/", (req, res) => {
  let xml = new Array()
  fs.readdir(testFolder, (err, files) => {
    files.forEach(file => {
      xml.push({ title: file })

      if(files.indexOf(file) == files.length - 1)
        res.send({xml})
    })
  })
})

app.get("/tags/:tags", (req, res) => {
  let req_tags = JSON.parse(req.params.tags)
  let xml = new Array()
  fs.readdir(testFolder,(err, files) => {
    files.forEach(async file => {
      let title = file.substring(0, file.length - 4)
      let jsonfile = "tags.json"
      let rawdata = fs.readFileSync(jsonfile)
      let tags = JSON.parse(rawdata)
      let file_tags = tags[title]
      if( ( req_tags.title == null ||  file.toLowerCase().includes(req_tags.title.toLowerCase()) ) &&
          ( req_tags.architecture == null || file_tags.architecture == req_tags.architecture ) &&
          ( req_tags["sub-architecture"] == null || file_tags["sub-architecture"] == req_tags["sub-architecture"]) &&
          ( req_tags["uname-architecture"] == null || file_tags["uname-architecture"] == req_tags["uname-architecture"]) &&
          ( req_tags["cpu-vendor"] == null || file_tags["cpu-vendor"] == req_tags["cpu-vendor"]) &&
          ( req_tags["cpu-family"] == null || file_tags["cpu-family"] == req_tags["cpu-family"]) &&
          ( req_tags["chassis"] == null || file_tags["chassis"] == req_tags["chassis"]) &&
          ( req_tags["PCI"] == null || file_tags["PCI"] == req_tags["PCI"] ) &&
          ( req_tags.nbcores == null || file_tags.nbcores >= req_tags.nbcores) &&
          ( req_tags.nbpackages == null || file_tags.nbpackages >= req_tags.nbpackages) &&
          ( req_tags.NUMA == null || file_tags.NUMA >= req_tags.NUMA ) &&
          ( req_tags.OSDevice == null || file_tags.OSDevice == req_tags.OSDevice ) &&
          ( req_tags["various-memory"] == null || file_tags["various-memory"] == req_tags["various-memory"] ) &&
          ( req_tags.year == null || file_tags.year >= req_tags.year ) )
          xml.push({ title: file })

        if(files.indexOf(file) == files.length - 1)
          res.send({xml})
    })
  })
})

app.get("/json", (req, res) => {
  let json = require(path.join(__dirname, "tags.json"))
  res.json(json)
})

fs.readdir(testFolder, async (err, files) => {
  files.forEach(file => {
    app.get("/xml/" + file, (req, res) => {
      const xml = path.join(__dirname, testFolder + file)
      res.download(xml)
    })
  })
})

const PORT = process.env.PORT || 5000;
app.listen(PORT, function() {
  console.log(`App listening on port ${PORT}`)
})
